<?php

/**
 * Part of CodeIgniter Simple and Secure Twig
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/codeigniter-ss-twig
 */
class TwigCI
{
    private $config = array(
        'paths' => array(VIEWPATH),
    );

    private $functions_asis = array(
        'base_url',
        'site_url',
    );
    private $functions_safe = array(
        'form_open',
        'form_close',
        'form_error',
        'set_value',
        'set_select',
        'set_checkbox',
        'form_hidden',
        'formatForView',
        'form_input',
        'form_open_multipart',
        'json_encode',
        'getDisplayStatusRegistro',
        'getOptionStatusRegistro',
        'getNameByUser',
        'getFilialByValue',
    );

    private $filters = array(
        'campoObrigatorio',
    );

    private $twig;
    private $loader;

    public function __construct($params = array())
    {
        $this->config = array_merge($this->config, $params);
    }

    public function createTwig()
    {
        if (ENVIRONMENT === 'production') {
            $debug = false;
        } else {
            $debug = true;
        }

        if ($this->loader === null) {
            $this->loader = new \Twig_Loader_Filesystem($this->config['paths']);
        }

        $twig = new \Twig_Environment($this->loader, array(
            'cache'      => APPPATH . '/cache/twig',
            'debug'      => $debug,
            'autoescape' => true,
        ));

        $twig->getExtension('core')->setDateFormat('d/m/Y', '%d days');

        if ($debug) {
            $twig->addExtension(new \Twig_Extension_Debug());
        }

        $this->twig = $twig;
        $this->addCIFunctions();
    }

    public function setLoader($loader)
    {
        $this->loader = $loader;
    }

    /**
     * Renders Twig Template and Set Output
     *
     * @param string $view template filename without `.twig`
     * @param array  $params
     */
    public function display($view, $params = array())
    {
        $CI =& get_instance();
        $CI->output->set_output($this->render($view, $params));
    }

    /**
     * Renders Twig Template and Returns as String
     *
     * @param string $view template filename without `.twig`
     * @param array  $params
     *
     * @return string
     */
    public function render($view, $params = array())
    {
        $this->createTwig();

        $view = $view . '.twig';

        return $this->twig->render($view, $params);
    }

    private function addCIFunctions()
    {
        // as is functions
        foreach ($this->functions_asis as $function) {
            if (function_exists($function)) {
                $this->twig->addFunction(
                    new \Twig_SimpleFunction(
                        $function,
                        $function
                    )
                );
            }
        }

        // safe functions
        foreach ($this->functions_safe as $function) {
            if (function_exists($function)) {
                $this->twig->addFunction(
                    new \Twig_SimpleFunction(
                        $function,
                        $function,
                        array('is_safe' => array('html'))
                    )
                );
            }
        }

        // customized functions
        if (function_exists('anchor')) {
            $this->twig->addFunction(
                new \Twig_SimpleFunction(
                    'anchor',
                    array($this, 'safe_anchor'),
                    array('is_safe' => array('html'))
                )
            );
        }

        if (function_exists('form_label')) {
            $this->twig->addFunction(
                new \Twig_SimpleFunction(
                    'form_label',
                    array($this, 'safe_label'),
                    array('is_safe' => array('html'))
                )
            );
        }
    }

    public function safe_label($idCampo, $texto, $obrigatorio = false)
    {
        $attributes = array();

        if ($obrigatorio) {
            $attributes['title'] = 'Campo obrigatório';
            $texto               = sprintf('%s <strong class="text-danger" title="Campo obrigatório">*</strong>', $texto);
        }

        return form_label($texto, $idCampo, $attributes);
    }

    /**
     * @param string $uri
     * @param string $title
     * @param array  $attributes [changed] only array is acceptable
     *
     * @return string
     */
    public function safe_anchor($uri = '', $title = '', $attributes = array())
    {
        $uri   = html_escape($uri);
        $title = html_escape($title);

        $new_attr = array();
        foreach ($attributes as $key => $val) {
            $new_attr[html_escape($key)] = html_escape($val);
        }

        return anchor($uri, $title, $new_attr);
    }

    /**
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }
}
