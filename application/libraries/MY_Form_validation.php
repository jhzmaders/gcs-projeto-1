<?php

class MY_Form_validation extends CI_Form_validation
{
    public function valid_date($date)
    {
        if (trim($date) === '') {
            return true;
        }

        $newDate = DateTime::createFromFormat('d/m/Y', $date);

        $lastErrors = $newDate->getLastErrors();

        return $lastErrors['warning_count'] === 0 && $lastErrors['error_count'] === 0;
    }

    public function date_greater_equals_than($endDate, $startDateFieldName)
    {
        $data = empty($this->validation_data) ? $_POST : $this->validation_data;

        $startDate = array_key_exists($startDateFieldName, $data) ? $data[$startDateFieldName] : '';

        if (trim($endDate) === '' || trim($startDate) === '') {
            return true;
        }

        $endDate   = DateTime::createFromFormat('d/m/Y', $endDate);
        $startDate = DateTime::createFromFormat('d/m/Y', $startDate);

        return $endDate >= $startDate;
    }
}