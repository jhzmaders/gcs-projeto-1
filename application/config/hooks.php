<?php

use toshyro\gcs\repository\RepositoryFactory;



defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'] = function () {
    //return true;

    if (is_cli()) {
        return true;
    }

    $ci = &get_instance();

    $isLogged   = $ci->session->logged;
    $module     = $ci->uri->segment(1);
    $controller = $ci->uri->segment(2);
    $method     = $ci->uri->segment(3);
    $post       = $ci->uri->segment(4);

    if ($module === 'sistema') {
        if ($controller === 'login') {
            return true;
        }

        if ($controller === "dashboard" && $isLogged) {
            return true;
        }
    }

    if ($isLogged) {
        $idUsuario = getIdUsuarioAutenticado();

        $usuario = RepositoryFactory::make('usuarios')->findByID($idUsuario);

        if ($usuario->tokenacesso) {
            redirect('sistema/perfil');
        }

        $modulosComPermissao = RepositoryFactory::make('modulos')
                                                ->modulosComPermissao($idUsuario);

        $moduloPermitido = array_filter($modulosComPermissao, function ($modulo) use ($module) {
            return $modulo->urisegment === $module;
        });

        if (count($moduloPermitido)) {
            return true;
        }

        redirect('sistema/dashboard');
    }

    redirect('sistema/login');
};
