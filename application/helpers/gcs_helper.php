<?php

use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('getUsuarioLogadoViewData')) {
    function getUsuarioLogadoViewData()
    {
        $ci       = &get_instance();
        $isLogged = $ci->session->logged;

        $viewData = array(
            'tag' => file_get_contents(APPPATH . '../release/tag'),
            'env' => '',
        );

        if (getenv('environment') !== 'production') {
            $viewData['env'] = ' - ' . getenv('environment');
        }

        if ($isLogged) {
            $idUsuario         = getIdUsuarioAutenticado();
            $usuario           = RepositoryFactory::make('usuarios')->findByID($idUsuario);
            $permissoes        = RepositoryFactory::make('usuariospermissoes')->getPermissoesByUsuario($idUsuario);
            $usuarioPermissoes = array();


            foreach ($permissoes as $permissao) {
                $usuarioPermissoes[$permissao->moduloid][] = $permissao->permissaovalor;
            }

            $viewData['usuarioAutenticado'] = $usuario;
            $viewData['usuarioPermissoes']  = $usuarioPermissoes;
        }

        return $viewData;
    }
}

if (!function_exists('getIdUsuarioAutenticado')) {
    function getIdUsuarioAutenticado()
    {
        $ci = &get_instance();

        if ($ci->session->logged) {

            $id = 1;

//            if (isset($ci->session->userdata('usuario')['id'])) {
//                $id = $ci->session->userdata('usuario')['id'];
//            }

            return $id;
        }

        return 1;
    }
}

if (!function_exists('getFilialByValue')) {
    function getFilialByValue($filial)
    {
        switch ($filial) {
            case 1:
                return "01 – Matriz";
                break;
            case 2:
                return "13 – Canoinhas";
                break;
            case 3:
                return "16 - São Mateus";
                break;
            case 4:
                return "26 – Prudentópolis";
                break;
            default:
                return "";
        }
    }
}