<?php
/**
 * Created by PhpStorm.
 * User: Jonas Maders
 * Date: 06/02/2019
 * Time: 09:26
 */

namespace toshyro\helpers;


class DownloadHelper
{
    public static function download($path)
    {
        $ci = &get_instance();

        $ci->load->helper('file');
        $ci->load->helper('download');


        if (get_mime_by_extension($path) == 'application/pdf') {
            $ci->output->set_header(sprintf('Content-Disposition: inline; filename="%s"',
                pathinfo($path, PATHINFO_BASENAME)))
                ->set_header('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->set_content_type('application/pdf')
                ->set_output(file_get_contents($path));
        } else {
            if (file_exists($path)) {
                $conteudo = file_get_contents($path);
                force_download(pathinfo($path, PATHINFO_BASENAME), $conteudo);
            }
        }
    }
}