<?php

use toshyro\gcs\repository\RepositoryFactory;
use toshyro\helpers\StatusRegistroHelper;

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('formatForView')) {
    function formatForView($date, $displayFormat = 'd/m/Y', $dbFormat = 'Y-m-d H:i:s')
    {
        if ($date === null || trim($date) === '') {
            return null;
        }

        if (is_string($date)) {
            $date = DateTime::createFromFormat($dbFormat, $date);
        }

        return $date->format($displayFormat);
    }
}

if (!function_exists('formatDateForDB')) {
    function formatDateForDB($date)
    {
        if ($date === null || trim($date) === '') {
            return null;
        }

        if (is_string($date)) {
            $date = DateTime::createFromFormat('d/m/Y', $date);
        }

        return $date->format('Y-m-d');
    }
}

if (!function_exists('formatDecimalForDB')) {
    function formatDecimalForDB($value)
    {
        if ($value === null || trim($value) === '') {
            return null;
        }

        if (is_string($value)) {
            $value = str_replace(',', '.', str_replace('.', '', $value));
        }

        return $value;
    }
}

if (!function_exists('getDisplayStatusRegistro')) {
    function getDisplayStatusRegistro($value)
    {
        if ($value === null || trim($value) === '') {
            return null;
        }

        return StatusRegistroHelper::getInstance()->getDisplayStatus($value);
    }
}

if (!function_exists('getNameByUser')) {
    function getNameByUser($value)
    {
        if ($value === null || trim($value) === '') {
            return null;
        }


        $retorno = array();
        $retorno['completo'] = RepositoryFactory::make('usuarios')->getNameByUserId($value)[0]->nome;
        $retorno['abrev'] = explode(' ', $retorno['completo'])[0];

        return $retorno;
    }
}

if (!function_exists('getOptionStatusRegistro')) {
    function getOptionStatusRegistro($value)
    {
        if ($value === null || trim($value) === '') {
            return null;
        }

        return StatusRegistroHelper::getOptionsStatus($value);
    }
}

if (!function_exists('isWeekend')) {
    function isWeekend($date) {
        $weekDay = date('w', strtotime($date));
        $t = ($weekDay == 0 || $weekDay == 6);
        return $t;
    }
}