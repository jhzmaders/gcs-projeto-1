<?php
/**
 * Created by PhpStorm.
 * User: Jonas Maders
 * Date: 13/02/2019
 * Time: 20:55
 */

namespace toshyro\helpers;


use DateTime;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;

class StatusRegistroHelper
{
    protected static $_instance;

    const STATUS_PENDENTE = 0;
    const STATUS_CONCLUIDO = 1;
    const STATUS_COM_PENDENCIA = 3;

    const OPTION_SUBSTATUS_CONCLUIDO_CONFORME = '1';
    const OPTION_SUBSTATUS_CONCLUIDO_INCONFORME = '2';

    const LABEL_SUBSTATUS_CONCLUIDO_CONFORME = 'Conforme';
    const LABEL_SUBSTATUS_CONCLUIDO_INCONFORME = 'Inconforme';

    const LABEL_SUBSTATUS_PENDENTE_EM_DIA = 'Em dia';
    const LABEL_SUBSTATUS_PENDENTE_A_VENCER = 'A vencer';
    const LABEL_SUBSTATUS_PENDENTE_VENCIDOS = 'Vencido';
    const LABEL_SUBSTATUS_PENDENTE_CONFORME = 'Conforme';
    const LABEL_SUBSTATUS_PENDENTE_INCONFORME = 'Inconforme';

    const LABEL_COM_PENDENCIAS = 'Com Pendência';

    const STATUS_COLOR_VENCIDO = 'status-vencido';
    const STATUS_COLOR_A_VENCER = 'status-a-vencer';
    const STATUS_COLOR_EM_DIA = 'status-em-dia';
    const STATUS_COLOR_CONCLUIDO = 'status-concluido';
    const STATUS_COLOR_COM_PENDENCIAS = 'status-com-pendencias';

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public static function getOptionsStatus($status){
        switch ($status) {
            case self::OPTION_SUBSTATUS_CONCLUIDO_CONFORME:
                return self::LABEL_SUBSTATUS_CONCLUIDO_CONFORME;
            case self::OPTION_SUBSTATUS_CONCLUIDO_INCONFORME:
                return self::LABEL_SUBSTATUS_CONCLUIDO_INCONFORME;
        }
    }

    public function getDisplayStatus($idRegistro)
    {
        $registro = RepositoryFactory::make('registros')->findByID($idRegistro);

        switch ($registro->status) {
            case self::STATUS_PENDENTE:
                return self::getSubstatusForPendente($registro);
            case self::STATUS_CONCLUIDO:
                return self::getSubstatusForConcluido($registro);
            case self::STATUS_COM_PENDENCIA:
                return self::LABEL_COM_PENDENCIAS;
        }
    }

    public function getColorStatusRegistro($registro)
    {
        $registroFilho = $this->getRegistroMenorDataNaoConcluido($registro->id);

        if($registroFilho->status == 3){
            $registro->color = self::STATUS_COLOR_COM_PENDENCIAS;
            return $registro;
        }

        $menorData = new DateTime($registroFilho->datavalidade);

        $today = new DateTime();

        $qtdDayAvencer = RepositoryFactory::make('parametros')->findByID('1');
        $todayLimit = (new DateTime())->modify('+1 day')->modify('+'.$qtdDayAvencer->avencer.' day');


        $registro->color = self::STATUS_COLOR_EM_DIA;
        if ($menorData <= $todayLimit) {
            $registro->color = self::STATUS_COLOR_A_VENCER;
        }

        if ($menorData <= $today) {
            $registro->color = self::STATUS_COLOR_VENCIDO;
        }

        if($registro->status == 1){
            $registro->color = self::STATUS_COLOR_CONCLUIDO;
        }

        return $registro;
    }

    public function getRegistroMenorDataNaoConcluido($idRegistro)
    {
        $registros[] = RepositoryFactory::make('registros')->findByID($idRegistro);

        $registrosFilhos = RepositoryFactory::make('registros')->findBy(
            array(
                new QueryCriteria('idpai', $idRegistro),
                new QueryCriteria('status', array(0, 3))
            ), array('dataconclusao' => 'desc', 'status' => 'desc')
        );

        $registros = array_merge($registros, $registrosFilhos);

        $registroResult = null;
        foreach ($registros as $registro) {
            $registro->datavalidade = new DateTime($registro->datavalidade);
            if ($registroResult === null || $registro->datavalidade < $registroResult->datavalidade ) {

                $registroResult = $registro;
            }
        }
        
        $registroResult->datavalidade = $registroResult->datavalidade->format('Y-m-d H:i:s');
        return $registroResult;
    }

    private function getSubstatusForPendente($registro){
        $menorRegistro = self::getRegistroMenorDataNaoConcluido($registro->id);

        if($menorRegistro->status === self::STATUS_COM_PENDENCIA){
            return self::LABEL_COM_PENDENCIAS;
        }

        $validade = new DateTime($menorRegistro->datavalidade);
        $today = new DateTime();

        $qtdDayAvencer = RepositoryFactory::make('parametros')->findByID('1');
        $atrasado = (new DateTime())->modify('+'.$qtdDayAvencer->avencer.' day');

        if($validade < $today){
            return self::LABEL_SUBSTATUS_PENDENTE_VENCIDOS;
        }

        if($validade < $atrasado){
            return self::LABEL_SUBSTATUS_PENDENTE_A_VENCER;
        }

        return self::LABEL_SUBSTATUS_PENDENTE_EM_DIA;
    }

    private function getSubstatusForConcluido($registro){
        if($registro->registroinconformidade){
            return self::LABEL_SUBSTATUS_CONCLUIDO_INCONFORME;
        }else{
            return self::LABEL_SUBSTATUS_CONCLUIDO_CONFORME;
        }
    }
}