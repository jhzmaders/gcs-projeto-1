<?php

namespace toshyro\gcs\model\validation;

class CodeIgniterValidationRulesBuilder
{
    private static $_instance;
    private        $validationRules;

    protected function __construct()
    {
        $this->clearRules();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new static;
        }

        return self::$_instance;
    }

    public function setRules($field, $label, array $rules)
    {
        $this->validationRules[$field] = array(
            'field' => $field,
            'label' => $label,
            'rules' => $rules,
        );

        return $this;
    }

    public function getRules()
    {
        $rules = $this->validationRules;

        $this->validationRules = array();

        return $rules;
    }

    public function clearRules()
    {
        $this->validationRules = array();

        return $this;
    }
}