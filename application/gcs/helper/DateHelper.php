<?php

namespace toshyro\gcs\helper;


class DateHelper
{

    const DATABASE_FORMAT_DATE     = 'Y-m-d';
    const DATABASE_FORMAT_DATETIME = 'Y-m-d H:i';
    const DISPLAY_FORMAT_DATE      = 'd/m/Y';
    const DISPLAY_FORMAT_DATETIME  = 'd/m/Y H:i';


    public static function formatForDatabase($value)
    {
        $date = '00/00/0000 00:00:00';

        if (self::getDateFormat($value) === self::DISPLAY_FORMAT_DATETIME && self::isValidDate($value)) {
            $date = self::format($value, self::DATABASE_FORMAT_DATETIME);
        }

        return $date;
    }

    public static function getDateFormat($date)
    {
        return strpos($date, '-') ? self::DATABASE_FORMAT_DATETIME : self::DISPLAY_FORMAT_DATETIME;
    }

    public static function isValidDate($date)
    {
        $format = self::getDateFormat($date) === self::DISPLAY_FORMAT_DATETIME ? self::DISPLAY_FORMAT_DATETIME : self::DATABASE_FORMAT_DATETIME;

        $dateObject  = \DateTime::createFromFormat($format, $date);
        $isValidDate = false;

        if ($dateObject) {
            $errors      = $dateObject->getLastErrors();
            $isValidDate = $errors['warning_count'] === 0 && $errors['error_count'] === 0;
        }

        return $isValidDate;
    }

    public static function format($value, $format)
    {
        setlocale(LC_TIME, 'pt_BR.utf8', 'pt_BR.iso88591', 'pt-BR');

        if ($value instanceof \DateTime || $value instanceof \DateTimeImmutable) {
            return $value->format($format);
        }

        return $value !== null ? date($format, strtotime(str_replace('/', '-', $value))) : null;
    }
}