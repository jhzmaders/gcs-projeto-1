<?php

namespace toshyro\gcs\service;

class PdfReportService
{
    const ORIENTACAO_PAISAGEM = 'L';
    const ORIENTACAO_RETRATO  = 'P';
    const HTML_BODY           = 2;
    const CSS_HEADER          = 1;

    private $ci;
    private $orientacao;
    private $modo;
    private $formato;
    private $tamanhoFonte;
    private $tipoFonte;
    private $margemEsquerda;
    private $margemDireita;
    private $margemSuperior;
    private $margemInferior;
    private $margemCabecalho;
    private $margemRodape;
    /** @var \mPDF $mpdf */
    private $mpdf;
    private $nomeArquivo;
    private $arquivoTemplate;
    private $caminhoArquivoFisico;
    private $textoMarcaDagua;

    public function __construct()
    {
        $this->ci = &get_instance();

        $this->modo            = 'utf-8';
        $this->formato         = 'A4';
        $this->tamanhoFonte    = 9;
        $this->tipoFonte       = 'Verdana';
        $this->margemEsquerda  = 15;
        $this->margemDireita   = 15;
        $this->margemSuperior  = 25;
        $this->margemInferior  = 10;
        $this->margemCabecalho = 8;
        $this->margemRodape    = 8;
        $this->orientacao      = self::ORIENTACAO_RETRATO;
        $this->textoMarcaDagua = 'PREVIEW';
    }

    public function setMarcaDagua($texto)
    {
        $this->textoMarcaDagua = $texto;

        return $this;
    }

    public function setCaminhoArquivoFisico($caminho)
    {
        $this->caminhoArquivoFisico = $caminho;

        return $this;
    }

    public function gerarPDF($html, $imprimeEmTela = true, $gerarArquivoFisico = false)
    {
        $localUpload = null;

        $this->mpdf = new \mPDF($this->modo, $this->formato, $this->tamanhoFonte, $this->tipoFonte,
            $this->margemEsquerda, $this->margemDireita,
            $this->margemSuperior, $this->margemInferior, $this->margemCabecalho, $this->margemRodape,
            $this->orientacao);

        $this->mpdf->simpleTables = true;
        $this->mpdf->packTableData = true;

        $this->mpdf->SetTitle($this->getNomeArquivo());

        $this->loadCSS();
        $header = '<div class="row"><div style="text-align: center"><h2>GCS</h2></div></div>';

        $this->mpdf->DefHeaderByName('cabecalho', $header);
        $this->mpdf->SetFooter('{PAGENO}');
        $this->mpdf->h2toc = array('H3' => 0, 'H4' => 1, 'H5' => 2);

        if (is_array($html)) {
            foreach ($html as $position => $page) {
                $this->mpdf->WriteHTML($page, self::HTML_BODY);
                if ($position < count($html) - 1) {
                    $this->mpdf->AddPage($this->orientacao);
                }
            }
        } else {
            $this->mpdf->WriteHTML($html, self::HTML_BODY);
        }

        if ($gerarArquivoFisico && is_dir($this->caminhoArquivoFisico)) {
            $this->mpdf->Output($this->getNomeArquivoTratado(), 'F');
        }

        if ($imprimeEmTela) {
            $this->mpdf->Output();
        }
    }

    private function getNomeArquivo()
    {
        return $this->nomeArquivo ?: sha1(time());
    }

    private function loadCSS()
    {
        $defaultCSS = APPPATH . '../public/css/base/base_relatorios_bootstrap.css';

        if (file_exists($defaultCSS)) {
            $this->mpdf->WriteHTML(file_get_contents($defaultCSS), self::CSS_HEADER);
        }
    }

    public function getNomeArquivoTratado()
    {
        return $this->trataNomeArquivo($this->getNomeArquivo()) . '.pdf';
    }

    private function trataNomeArquivo($nomeArquivo)
    {
        $de      = 'áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ';
        $para    = 'aaaaeeiooouucAAAAEEIOOOUUC_';
        $chaves  = array();
        $valores = array();
        preg_match_all('/./u', $de, $chaves);
        preg_match_all('/./u', $para, $valores);
        $mapping = array_combine($chaves[0], $valores[0]);

        return strtr($nomeArquivo, $mapping);
    }

    public function setNomeArquivo($nomeArquivo)
    {
        $this->nomeArquivo = $nomeArquivo;

        return $this;
    }

    public function getBase64PdfStream()
    {
        $pdf = null;

        if (file_exists($this->getNomeArquivoTratado())) {
            $pdf = file_get_contents($this->getNomeArquivoTratado());
        } else {
            throw new \RuntimeException('O arquivo ' . $this->getNomeArquivoTratado() . ' nao foi encontrado no servidor');
        }

        return base64_encode($pdf);
    }

    public function setTemplate($caminhoTemplate)
    {
        if (file_exists($caminhoTemplate)) {
            $this->arquivoTemplate = $caminhoTemplate;
        }

        return $this;
    }

    public function getOrientacao()
    {
        return $this->orientacao;
    }

    public function setOrientacao($orientacao)
    {
        $this->orientacao = $orientacao;

        if ($orientacao === self::ORIENTACAO_PAISAGEM) {
            $this->setFormato('A4-' . self::ORIENTACAO_PAISAGEM);
        } else {
            $this->setFormato('A4');
        }

        return $this;
    }

    public function getModo()
    {
        return $this->modo;
    }

    public function setModo($modo)
    {
        $this->modo = $modo;

        return $this;
    }

    public function getFormato()
    {
        return $this->formato;
    }

    public function setFormato($formato)
    {
        $this->formato = $formato;

        return $this;
    }

    public function getTamanhoFonte()
    {
        return $this->tamanhoFonte;
    }

    public function setTamanhoFonte($tamanho)
    {
        $this->tamanhoFonte = $tamanho;

        return $this;
    }

    public function getTipoFonte()
    {
        return $this->tipoFonte;
    }

    public function setTipoFonte($tipoFonte)
    {
        $this->tipoFonte = $tipoFonte;

        return $this;
    }

    public function getMargemEsquerda()
    {
        return $this->margemEsquerda;
    }

    public function setMargemEsquerda($margem)
    {
        $this->margemEsquerda = $margem;

        return $this;
    }

    public function getMargemDireita()
    {
        return $this->margemDireita;
    }

    public function setMargemDireita($margem)
    {
        $this->margemDireita = $margem;

        return $this;
    }

    public function getMargemSuperior()
    {
        return $this->margemSuperior;
    }

    public function setMargemSuperior($margem)
    {
        $this->margemSuperior = $margem;

        return $this;
    }

    public function getMargemInferior()
    {
        return $this->margemInferior;
    }

    public function setMargemInferior($margem)
    {
        $this->margemInferior = $margem;

        return $this;
    }

    public function getMargemCabecalho()
    {
        return $this->margemCabecalho;
    }

    public function setMargemCabecalho($margem)
    {
        $this->margemCabecalho = $margem;

        return $this;
    }

    public function getMargemRodape()
    {
        return $this->margemRodape;
    }

    public function setMargemRodape($margem)
    {
        $this->margemRodape = $margem;

        return $this;
    }
}