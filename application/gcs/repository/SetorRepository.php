<?php

namespace toshyro\gcs\repository;

class SetorRepository extends GenericRepository
{
    public function getSetoresByUsuario($idUsuario){
        $qb = $this->connection->createQueryBuilder();

        $qb->select('setores.id id, setores.descricao descricao, setores.ativo ativo')
           ->from('usuarios', 'usuario')
           ->join('usuario', 'usuariossetores', 'usuariossetores', 'usuario.id = usuariossetores.idUsuario')
           ->join('usuariossetores', 'setores', 'setores', 'setores.id = usuariossetores.idSetor')
           ->andWhere('usuario.id = :idusuario')
           ->andWhere('usuario.ativo = true')
           ->setParameter('idusuario', $idUsuario);

        return $qb->execute()->fetchAll();
    }

    public function getDescricaoSetoresByUsuario($idUsuario){
        $qb = $this->connection->createQueryBuilder();

        $qb->select('setores.descricao descricao')
           ->from('usuarios', 'usuario')
           ->join('usuario', 'usuariossetores', 'usuariossetores', 'usuario.id = usuariossetores.idusuario')
           ->join('usuariossetores', 'setores', 'setores', 'setores.id = usuariossetores.idsetor')
           ->andWhere('usuario.id = :idusuario')
           ->andWhere('usuario.ativo = true')
           ->setParameter('idusuario', $idUsuario);

        return $qb->execute()->fetchAll();
    }
}