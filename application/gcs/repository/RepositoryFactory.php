<?php

namespace toshyro\gcs\repository;

use toshyro\gcs\connection\ConnectionManager;

class RepositoryFactory
{
    private static $_instance;

    /**
     * @param $tableName
     *
     * @return IRepository
     */
    public static function make($tableName)
    {
        if (self::$_instance === null) {
            self::$_instance = new static;
        }

        return self::$_instance->getRepository($tableName);
    }

    /**
     * @param $tableName
     *
     * @return IRepository
     */
    private function getRepository($tableName)
    {
        $repository = null;
        $conn       = ConnectionManager::getInstance()->getConnection();

        switch ($tableName) {
            case 'log':
                $repository = new LogRepository($conn, $tableName);
                break;
            case 'modulos':
                $repository = new ModuloRepository($conn, $tableName);
                break;
            case 'usuarios':
                $repository = new UsuarioRepository($conn, $tableName);
                break;
            case 'usuariospermissoes':
                $repository = new UsuarioPermissaoRepository($conn, $tableName);
                break;
            default:
                $repository = new GenericRepository($conn, $tableName);
        }

        return $repository;
    }
}