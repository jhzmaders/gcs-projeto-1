<?php

namespace toshyro\gcs\repository;

interface IRepository
{
    public function findByID($id);

    public function findBy($criteria, $orderBy = null);

    public function findAll($orderBy = null);

    public function findOneBy($criteria, $orderBy = null);

    public function deleteBy($criteria);

    public function deleteByID($id);

    public function insert($data);

    public function update($data);

    public function getTableName();
}