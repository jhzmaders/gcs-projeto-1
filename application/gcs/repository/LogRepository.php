<?php

namespace toshyro\gcs\repository;

use Doctrine\DBAL\Connection;

class LogRepository extends GenericRepository
{
    protected $maxRows;

    public function __construct(Connection $connection, $tableName)
    {
        parent::__construct($connection, $tableName);
        $this->maxRows = 10;
    }

    public function setMaxRows($maxRows)
    {
        $this->maxRows = $maxRows;
    }

    public function findBy($criteria, $orderBy = null)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('x.*, y.nome as usuarionome, y.email as usuarioemail')
           ->from($this->tableName, 'x')
           ->join('x', 'usuarios', 'y', 'x.idusuario = y.id');

        $this->buildCriteria($qb, $criteria);
        $this->buildOrderBy($qb, $orderBy);
        $qb->setMaxResults($this->maxRows);

        return $qb->execute()->fetchAll();
    }

    public function findByID($id)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('x.*, y.nome as usuarionome, y.email as usuarioemail')
           ->from($this->tableName, 'x')
           ->join('x', 'usuarios', 'y', 'y.id = x.idusuario');

        $criteria = array(
            new QueryCriteria('x.id', $id),
        );

        $this->buildCriteria($qb, $criteria);

        $exec = $qb->execute();

        return $exec->rowCount() === 1 ? $exec->fetch() : null;
    }

    protected function gravarLogInclusao($novo, $observacoes = null)
    {
        return false;
    }

    protected function gravarLogAlteracao($original, $atualizado, $observacoes = null)
    {
        return false;
    }

    protected function gravarLogExclusao($registroOriginal, $observacoes = null)
    {
        return false;
    }
}