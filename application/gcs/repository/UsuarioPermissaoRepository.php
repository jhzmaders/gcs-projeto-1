<?php

namespace toshyro\gcs\repository;


class UsuarioPermissaoRepository extends GenericRepository
{
    public function getPermissoesByUsuario($usuario)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('permissao.id as permissaoId, permissao.descricao as permissaoDescricao, permissao.valor as permissaoValor')
           ->addSelect('modulo.id as  moduloId, modulo.descricao as moduloDescricao')
           ->from($this->tableName, 'usuarioPermissao')
           ->join('usuarioPermissao', 'permissoes', 'permissao', 'permissao.id = usuarioPermissao.idPermissao')
           ->join('permissao', 'modulos', 'modulo', 'modulo.id = permissao.idModulo')
           ->andWhere('usuarioPermissao.idUsuario = :usuario')
           ->setParameter('usuario', $usuario);

        return $qb->execute()->fetchAll();
    }

    public function getUsuariosByPermissao($permissao)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('usuario.id, usuario.nome')
           ->from($this->tableName, 'usuarioPermissao')
           ->join('usuarioPermissao', 'permissoes', 'permissao', 'permissao.id = usuarioPermissao.idPermissao')
           ->join('permissao', 'modulos', 'modulo', 'modulo.id = permissao.idModulo')
           ->join('usuarioPermissao', 'usuarios', 'usuario', 'usuarioPermissao.idUsuario = usuario.id')
           ->andWhere('permissao.id = :permissao')
           ->setParameter('permissao', $permissao);

        return $qb->execute()->fetchAll();
    }

}