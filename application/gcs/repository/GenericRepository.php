<?php

namespace toshyro\gcs\repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use toshyro\gcs\IObserver;
use toshyro\gcs\LogWriter;

class GenericRepository implements IRepository
{
    /** @var Connection */
    protected $connection;
    protected $tableName;
    protected $logWriter;
    protected $observers;

    public function __construct(Connection $connection, $tableName)
    {
        $this->connection = $connection;
        $this->tableName  = $tableName;
        $this->observers  = array();

//        $this->logWriter = LogWriter::getInstance();
    }

    public function findByID($id)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('*')
           ->from($this->tableName, 'x');

        $criteria = array(
            new QueryCriteria('x.id', $id),
        );

        $this->buildCriteria($qb, $criteria);

        $exec = $qb->execute();

        return $exec->rowCount() === 1 ? $exec->fetch() : null;
    }

    public function findBy($criteria, $orderBy = null)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('*')
           ->from($this->tableName, 'x');

        $this->buildCriteria($qb, $criteria);
        $this->buildOrderBy($qb, $orderBy);

        return $qb->execute()->fetchAll();
    }

    public function findAll($orderBy = null)
    {
        return $this->findBy(null, $orderBy);
    }

    public function findOneBy($criteria, $orderBy = null)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select('*')
           ->from($this->tableName, 'x');

        $this->buildCriteria($qb, $criteria);

        $this->buildOrderBy($qb, $orderBy);

        $exec = $qb->execute();

        return $exec->rowCount() === 1 ? $exec->fetch() : null;
    }

    public function deleteBy($criteria)
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->delete($this->tableName);

        $this->buildCriteria($qb, $criteria);

        return $qb->execute();

    }

    public function deleteByID($id)
    {
        $registroOriginal = $this->findByID($id);

        $criteria = array(new QueryCriteria('id', $id));

        $this->deleteBy($criteria);

        $this->gravarLogExclusao($registroOriginal);
    }

    public function insert($data)
    {
        if ($data) {
            foreach ($data as $key => &$valor) {
                $valor = trim($valor) !== '' ? trim($valor) : null;
            }
        }

        $qb = $this->connection->createQueryBuilder();

        $qb->insert($this->tableName);

        foreach ($data as $field => $value) {
            $param = $this->getNamedParameter($field);
            $qb->setValue($field, $param);
            $qb->setParameter($param, $value);
        }

        $qb->execute();

        $lastId = $this->connection->lastInsertId();

        $novoRegistro = $this->findByID($lastId);

        $this->gravarLogInclusao($novoRegistro);

        return $lastId;
    }

    public function update($data)
    {
        $id = (int)$data['id'];

        $antigo = $this->findByID($id);

        $qb = $this->connection->createQueryBuilder();

        $qb->update($this->tableName, 'x');

        $criteria = array(
            new QueryCriteria('x.id', $id),
        );

        $this->buildCriteria($qb, $criteria);

        foreach ($data as $field => $value) {
            $param = $this->getNamedParameter($field);
            $qb->set($field, $param);
            $qb->setParameter($param, $value);
        }

        $qb->execute();

        $atualizado = $this->findByID($id);

        $this->gravarLogAlteracao($antigo, $atualizado);
    }

    public function attach(IObserver $observer)
    {
        $this->observers[get_class($observer)] = $observer;
    }

    public function detach(IObserver $observer)
    {
        unset($this->observers[get_class($observer)]);
    }

    protected function gravarLogInclusao($novo, $observacoes = null)
    {
//        $mensagemLog = $this->serializeDataToLog($novo);
//
//        $this->logWriter->setTipo(LogWriter::TIPO_LOG_INSERCAO)
//                        ->setIdReferencia($novo->id)
//                        ->setMensagem($mensagemLog)
//                        ->setPrograma($this->tableName)
//                        ->setObservacoes($observacoes)
//                        ->write();
    }

    protected function gravarLogAlteracao($original, $atualizado, $observacoes = null)
    {
//        $mensagemLog = $this->serializeDataToLog($atualizado, $original);
//
//        if (trim($mensagemLog) !== '') {
//            $this->logWriter->setTipo(LogWriter::TIPO_LOG_ALTERACAO)
//                            ->setIdReferencia($atualizado->id)
//                            ->setMensagem($mensagemLog)
//                            ->setPrograma($this->tableName)
//                            ->setObservacoes($observacoes)
//                            ->write();
//        }
    }

    protected function gravarLogExclusao($registroOriginal, $observacoes = null)
    {
//        $mensagemLog = $this->serializeDataToLog($registroOriginal);
//
//        $this->logWriter->setIdReferencia($registroOriginal->id)
//                        ->setTipo(LogWriter::TIPO_LOG_EXCLUSAO)
//                        ->setPrograma($this->tableName)
//                        ->setMensagem($mensagemLog)
//                        ->setObservacoes($observacoes)
//                        ->write();
    }

    protected function serializeDataToLog($data, $oldData = null)
    {
        $serializedData = '';

        foreach ($data as $campo => $valor) {
            if (strpos(strtolower($campo), 'ativo') !== false) {
                if ($valor == "1") {
                    $valor = "Sim";
                } else {
                    $valor = "Não";
                }


                if ($oldData) {
                    if ($oldData->{$campo} == "1") {
                        $oldData->{$campo} = "Sim";
                    } else {
                        $oldData->{$campo} = "Não";
                    }
                }
            }

            if ($oldData) {
                if ($valor != $oldData->{$campo}) {
                    $serializedData .= sprintf("%s%s = \"%s\" (estava \"%s\")", PHP_EOL, $campo, $valor,
                        $oldData->{$campo});
                }
                continue;
            }

            $serializedData .= sprintf("%s%s = %s", PHP_EOL, $campo, $valor);
        }

        return strlen($serializedData) > 0 ? substr($serializedData, 1) : '';
    }

    protected function buildCriteria(QueryBuilder $qb, $criteria)
    {
        if ($criteria) {
            /** @var QueryCriteria $queryCriteria */
            foreach ($criteria as $queryCriteria) {
                $paramName = $this->getNamedParameter($queryCriteria->getField());

                $expr = $qb->expr();

                if (is_array($queryCriteria->getValue())) {

                    switch ($queryCriteria->getOperator()) {
                        case 'likeIn':
                            $expr = $qb->expr()->in($queryCriteria->getField(), $paramName);
                            break;
                        case 'notIn':
                            $expr = $qb->expr()->notIn($queryCriteria->getField(), array($paramName));
                            break;
                        default:
                            $expr = $qb->expr()->in($queryCriteria->getField(), array($paramName));
                    }
                    $qb->andWhere($expr);
                    $qb->setParameter($paramName, $queryCriteria->getValue(), Connection::PARAM_STR_ARRAY);
                    continue;
                }

                switch ($queryCriteria->getOperator()) {
                    case '<>':
                        $expr = $expr->neq($queryCriteria->getField(), $paramName);
                        break;
                    case '>':
                        $expr = $expr->gt($queryCriteria->getField(), $paramName);
                        break;
                    case '<':
                        $expr = $expr->lt($queryCriteria->getField(), $paramName);
                        break;
                    case '>=':
                        $expr = $expr->gte($queryCriteria->getField(), $paramName);
                        break;
                    case '<=':
                        $expr = $expr->lte($queryCriteria->getField(), $paramName);
                        break;
                    case 'isNull':
                        $expr = $expr->isNull($queryCriteria->getField());
                        break;
                    case 'isNotNull':
                        $expr = $expr->isNotNull($queryCriteria->getField());
                        break;
                    case 'like':
                        $expr = $expr->like($queryCriteria->getField(), $paramName);
                        break;
                    default:
                        $expr = $expr->eq($queryCriteria->getField(), $paramName);
                }

                $qb->andWhere($expr);
                $qb->setParameter($paramName, $queryCriteria->getValue());
            }
        }
    }

    protected function buildOrderBy(QueryBuilder $qb, $orderBy)
    {
        if ($orderBy) {
            foreach ($orderBy as $field => $order) {
                $qb->addOrderBy($field, $order);
            }
        }
    }

    protected function getNamedParameter($fieldName)
    {
        return sprintf(':%s', str_replace('.', '_', $fieldName));
    }

    public function getTableName()
    {
        return $this->tableName;
    }
}