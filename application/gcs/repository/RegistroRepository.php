<?php

namespace toshyro\gcs\repository;


use DateTime;
use toshyro\helpers\StatusRegistroHelper;

class RegistroRepository extends GenericRepository
{
    public function insert($data){
        $ci           = &get_instance();
        $idUserLogado = getIdUsuarioAutenticado();

        $data['renovavel'] = isset($data['renovavel']) ? 1 : 0;
        $data['ativo']        = 1;
        $data['datavalidade'] = isset($data['datavalidade']) ? formatDateForDB($data['datavalidade']) : null;
        $data['proprietario'] = $idUserLogado;
        $ci->session->set_flashdata('pedirMotivo', true);


        if (!isset($data['idpai'])) {
            $usruarioLogado = RepositoryFactory::make('usuarios')->findByID($idUserLogado);

            $data['envolvidos'] = $usruarioLogado->email;
        }

        $id = parent::insert($data);

        $notificacoesPadrao = RepositoryFactory::make('notificacoespadroes')->findAll();
        foreach ($notificacoesPadrao as $notificacao) {
            $notificacao->datavalidade = (new DateTime($data['datavalidade']))->modify('-' . $notificacao->diasantes . 'day')->format('Y-m-d');
            $notificacao->idregistro   = $id;
            unset($notificacao->diasantes);
            unset($notificacao->id);

            if( new DateTime($notificacao->datavalidade) > new DateTime()){
                RepositoryFactory::make('notificacoes')->insert($notificacao);
            }
        }

        $notificacoesRegistro = RepositoryFactory::make('notificacoes')->findBy(array(
            new QueryCriteria('idregistro', $id),
        ));

        foreach ($notificacoesRegistro as $notificacao) {
            $weekend = isWeekend($notificacao->datavalidade);

            if ($weekend) {
                $ci->session->set_flashdata('isWeekend', true);
                break;
            }
        }

        return $id;
    }

    public function update($data, $observacoes = null)
    {
        $id = (int) $data['id'];

        $antigo = $this->findByID($id);

        $qb = $this->connection->createQueryBuilder();
        $qb->update($this->tableName, 'x');

        $criteria = array(
            new QueryCriteria('x.id', $id),
        );

        $this->buildCriteria($qb, $criteria);

        foreach ($data as $field => $value) {
            $param = $this->getNamedParameter($field);
            $qb->set($field, $param);
            $qb->setParameter($param, $value);
        }

        $qb->execute();

        $atualizado = $this->findByID($id);

        $this->gravarLogAlteracao($antigo, $atualizado, $observacoes);
    }

    // atalhos

    public function atualizaSequeciaByRegistro($idPai, $arrayIdInOrder){
        foreach ($arrayIdInOrder as $key => $idInOrder){
            $data['id'] = $idInOrder;
            $data['sequencia'] = $key + 1;

            $this->update($data);
        }
    }

    public function atualizaSequeciaRegistroByVencimento($idPai){
        $etapas = RepositoryFactory::make('registros')->findBy(
            array(new QueryCriteria('idpai', $idPai)),
            array('datavalidade' => 'asc', 'titulo' => 'asc')
        );

        foreach ($etapas as $key => $etapa){
            $data['id'] = $etapa->id;
            $data['sequencia'] = $key + 1;

            $this->update($data);
        }
    }

    public function atualizaSequeciaRegistroByTitulo($idPai){
        $etapas = RepositoryFactory::make('registros')->findBy(
            array(new QueryCriteria('idpai', $idPai)),
            array('titulo' => 'asc')
        );

        foreach ($etapas as $key => $etapa){
            $data['id'] = $etapa->id;
            $data['sequencia'] = $key + 1;

            $this->update($data);
        }
    }

    //get registros para cards

    public function getRegistrosByUsuario($userLogado)
    {

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, 
                     registros.datavalidade datavalidade, 
                     registros.titulo titulo,
                     registros.filial filial, 
                     registros.setor setor, 
                     registros.envolvidos envolvidos, 
                     registros.descricao descricao, 
                     registros.idpai idpai, 
                     registros.ativo ativo, 
                     registros.status status,
                     registros.dataconclusao conclusao, 
                     registros.registroinconformidade inconformidade,
                     registros.proprietario, 
 
                     (  
                        SELECT COUNT (etapas.id)
                        FROM registros AS etapas
                        WHERE etapas.idpai = registros.id
                     )
                     as totaletapas,
                     
                     (
                        SELECT COUNT (etapas.id)
                        FROM registros AS etapas
                        WHERE etapas.idpai = registros.id
                          AND etapas.status =  1
                     )
                     as totalconcluidas,
                     
                     (
                        SELECT COUNT (arquivos.id)
                        FROM registros AS etapas
                        INNER JOIN arquivos ON (arquivos.idregistro = etapas.id)
                        WHERE etapas.idpai = registros.id
                           OR ( etapas.id = registros.id AND etapas.idpai is null )
                     ) 
                     as anexototal' )
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.idpai is null')
           ->groupBy('registros.id');

        $qb->andWhere(self::getBySetoresEnvolvidosPermitidos($userLogado, $qb));

        return $qb->execute()->fetchAll();
    }

    public function getRegistrosPendentesByUsuario($userLogado)
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.idpai is null')
           ->andWhere('registros.status = :status')
           ->setParameter('status', StatusRegistroHelper::STATUS_PENDENTE);


        $qb->andWhere(self::getBySetoresEnvolvidosPermitidos($userLogado, $qb));

        return $qb->execute()->fetchAll();
    }

    public function getRegistrosConcluidosByUsuario($userLogado)
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.idpai is null')
           ->andWhere('registros.status != :status')
           ->setParameter('status', StatusRegistroHelper::STATUS_PENDENTE);

        $qb->andWhere(self::getBySetoresEnvolvidosPermitidos($userLogado, $qb));

        return $qb->execute()->fetchAll();
    }

    //get etapas para cards

    public function getEtapasVencidasByRegistros($ids)
    {
        if(!$ids)
            return array();

        $today = formatDateForDB((new DateTime())->format('d/m/Y'));

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.status = :status')
           ->andWhere('registros.datavalidade < :data')
           ->setParameter('status', StatusRegistroHelper::STATUS_PENDENTE)
           ->setParameter('data', $today);

        $qb->andWhere($qb->expr()->in('registros.idpai', $ids));

        return $qb->execute()->fetchAll();
    }

    public function getEtapasAvencerByRegistros($ids)
    {
        if(!$ids)
            return array();

        $parametro  = RepositoryFactory::make('parametros')->findByID(1);
        $prazo = formatDateForDB((new DateTime())->modify('+' . $parametro->avencer . ' day')->format('d/m/Y'));

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.status = :status')
           ->andWhere('registros.datavalidade <= :data')
           ->setParameter('status', StatusRegistroHelper::STATUS_PENDENTE)
           ->setParameter('data', $prazo);

        $qb->andWhere($qb->expr()->in('registros.idpai', $ids));

        return $qb->execute()->fetchAll();
    }

    public function getEtapasEmdiaByRegistros($ids)
    {
        if(!$ids)
            return array();

        $parametro  = RepositoryFactory::make('parametros')->findByID(1);
        $prazo = formatDateForDB((new DateTime())->modify('+' . $parametro->avencer . ' day')->format('d/m/Y'));

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.status = :status')
           ->andWhere('registros.datavalidade > :data')
           ->setParameter('status', StatusRegistroHelper::STATUS_PENDENTE)
           ->setParameter('data', $prazo);

        $qb->andWhere($qb->expr()->in('registros.idpai', $ids));

        return $qb->execute()->fetchAll();
    }

    public function getEtapasConcluidasByRegistros($ids)
    {
        if(!$ids)
            return array();

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.status = :status')
           ->setParameter('status', StatusRegistroHelper::STATUS_CONCLUIDO);

        $qb->andWhere($qb->expr()->in('registros.idpai', $ids));

        return $qb->execute()->fetchAll();
    }

    public function getEtapasComPendenciaByRegistros($ids)
    {
        if(!$ids)
            return array();

        $qb = $this->connection->createQueryBuilder();
        $qb->select('registros.id id, registros.datavalidade datavalidade, 
                     registros.titulo titulo, registros.filial filial, 
                     registros.setor setor, registros.envolvidos envolvidos, 
                     registros.descricao descricao, registros.idpai idpai, 
                     registros.ativo ativo, registros.status status,
                     registros.dataconclusao conclusao, registros.registroinconformidade inconformidade,
                     registros.proprietario')
           ->from($this->tableName, 'registros')
           ->where('registros.ativo = true')
           ->andWhere('registros.status = :status')
           ->setParameter('status', StatusRegistroHelper::STATUS_COM_PENDENCIA);

        $qb->andWhere($qb->expr()->in('registros.idpai', $ids));

        return $qb->execute()->fetchAll();
    }

    //privates

    private function getBySetoresEnvolvidosPermitidos($userLogado, $qb){
        $setorUsuario   = RepositoryFactory::make('setores')->getDescricaoSetoresByUsuario($userLogado->id);

        $orStatements = $qb->expr()->orX();
        $orStatements->add(
            $qb->expr()->like('registros.envolvidos', $qb->expr()->literal('%' . $userLogado->email . '%') )
        );

        foreach ($setorUsuario as $setor) {
            $orStatements->add(
                $qb->expr()->like('registros.setor',  $qb->expr()->literal( '%' .  $setor->descricao . '%') )
            );
        }

        $orStatements->add(
            $qb->expr()->eq('registros.proprietario', $userLogado->id)
        );

        return $orStatements;
    }
}