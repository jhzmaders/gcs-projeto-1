<?php

namespace toshyro\gcs;

class ObservableUpdater implements IObservable
{
    protected $observers;

    public function __construct()
    {
        $this->observers = array();
    }

    public function attach(IObserver $observer)
    {
        $this->observers[get_class($observer)] = $observer;
    }

    public function detach(IObserver $observer)
    {
        unset($this->observers[get_class($observer)]);
    }

    public function notify()
    {
        /** @var IObserver $attachedObservers */
        foreach ($this->observers as $attachedObservers) {
            $attachedObservers->update();
        }
    }
}