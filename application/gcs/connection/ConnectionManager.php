<?php

namespace toshyro\gcs\connection;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class ConnectionManager
{
    private        $connection;
    private static $_instance;

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
            self::$_instance->createConnection();
        }

        return self::$_instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    private function createConnection()
    {
        $config = new Configuration();

        $connectionParams = array(
            'url' => sprintf('postgresql://%s:%s@%s/%s?charset=utf8', getenv('dbuser'), getenv('dbpass'),
                getenv('dbhost'), getenv('dbname')),
        );

        $this->connection = DriverManager::getConnection($connectionParams, $config);
        $this->connection->setFetchMode(\PDO::FETCH_OBJ);
    }

}