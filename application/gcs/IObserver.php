<?php

namespace toshyro\gcs;

interface IObserver
{
    public function update();
}