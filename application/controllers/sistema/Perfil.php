<?php

use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\RepositoryFactory;

class Perfil extends BaseController
{
    public function index()
    {

        $idUsuario = getIdUsuarioAutenticado();
        if ($this->input->post()) {

            $requestData = array(
                'id'    => $idUsuario,
                'nome'  => $this->input->post('nome'),
                'email' => $this->input->post('email'),
            );

            if ($this->input->post('senhaatual')) {
                $usuario = RepositoryFactory::make('usuarios')->findByID($idUsuario);

                if ($usuario->tokenacesso !== $this->input->post('senhaatual') && !password_verify($this->input->post('senhaatual'),
                        $usuario->senha)) {

                    $this->twigError('sistema/perfil/perfil', array('errorMessage' => 'Senha não confere.'));

                    return;
                }

                if ($this->input->post('novasenha') !== $this->input->post('confirmasenha')) {
                    $this->twigError('sistema/perfil/perfil', array('errorMessage' => 'Senhas não são idênticas.'));

                    return;
                }

                $requestData['tokenacesso'] = null;
                $requestData['senha']       = password_hash($this->input->post('novasenha'), PASSWORD_DEFAULT);
            }

            RepositoryFactory::make('usuarios')->atualizaPerfil($requestData);

            $this->twigSuccess('sistema/perfil/perfil');

            return;
        }else{
            $usuario = RepositoryFactory::make('usuarios')->findByID($idUsuario);

            $viewData = array();

            if($usuario->tokenacesso){
                $viewData['serverAlert'] = 'É preciso trocar a senha';
            }
        }

        $this->twigDisplay('sistema/perfil/perfil', $viewData);
    }
}