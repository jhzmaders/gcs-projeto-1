<?php

use toshyro\gcs\model\validation\CodeIgniterValidationRulesBuilder;
use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;
use toshyro\gcs\service\ResetPasswordService;

class Login extends BaseController
{
    public function index()
    {
        if ($this->session->logged) {
            redirect('sistema/dashboard');
        }

        if ($this->input->post()) {
            $login = $this->input->post('login');
            $senha = $this->input->post('senha');

            $usuario = RepositoryFactory::make('usuarios')->findOneBy(array(
                new QueryCriteria('email', $login),
                new QueryCriteria('ativo', true),
            ));


            if ($usuario) {
                if (password_verify($senha, $usuario->senha) || $usuario->tokenacesso === $senha) {
                    $this->session->set_userdata('logged', true);
                    $this->session->set_userdata('usuario', array('id' => $usuario->id));


                    $url = $this->input->post('redirect_url');
                    if($url){
                        redirect($url, 'refresh');
                    }
                    redirect('sistema/dashboard');
                }
            }

            $this->twigError('sistema/login/login', array('errorMessage' => 'Usuário ou senha inválidos'));
        } else {
            $this->twigDisplay('sistema/login/login', array('redirect' =>$this->session->flashdata('redirect_url')));
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();

        redirect('sistema/login');
    }

    public function forgotPassword()
    {
        if ($this->input->post()) {
            try {

                if ($this->input->is_ajax_request() === false) {
                    exit('No direct script access allowed');
                }

                $data = $this->input->post();
                $this->validateRequestDataForgotPassword();

                $usuario = RepositoryFactory::make('usuarios')->findOneBy(array(
                        new QueryCriteria('email', $data['email']),
                        new QueryCriteria('ativo', true),
                    ));

                if(!$usuario){
                    return $this->ajaxOutput(null,'Usuário não existe ou não encontrado, verifique o email digitado');
                }

                $resetPasswordService = new ResetPasswordService();
                $resetPasswordService->sendResetPasswordTokenTo($usuario);

                $viewData['serverSuccess'] = "Pronto! Enviamos um e-mail para { $usuario->email } com as instruções para redefinir a senha.";

                return $this->ajaxOutput($viewData);

            } catch (\Exception $error) {
                return $this->ajaxOutput(null,$error->getMessage());
            }
        }

        $content = $this->load->view('sistema/login/login_esqueceu_senha', null, true);
        $footer  = $this->load->view('sistema/login/login_esqueceu_senha_partial_toolbar', null, true);

        $this->outputToModal('Recuperar Senha', $content, $footer, array('modalSize' => 'modal-small'));
    }

    public function resetPassword($token)
    {
        $viewData = array(
            'token'   => $token
        );

        try {
            $usuario = RepositoryFactory::make('usuarios')->findOneBy(
                array(
                    new QueryCriteria('tokenacesso', $token),
                    new QueryCriteria('ativo', true),
                )
            );

            $viewData['name']   = $usuario->nome;
            $viewData['email']  = $usuario->email;

            if ($this->input->post()) {
                $data = $this->input->post();
                self::validateRequestDataPasswordsMatches();

                $usuario->alterarSenha = '1';
                $usuario->senha = $data['senha'];
                $usuario->confirmasenha = $data['confirmasenha'];

                $this->usuarioRepository->update(get_object_vars($usuario));

                redirect('sistema/dashboard');
            }

        } catch (\Exception $error) {
            $viewData['errorMessage'] = $error->getMessage();

            $this->twigError('sistema/login/login_esqueceu_senha_novasenha', $viewData);
            return false;
        }

        $this->twigDisplay('sistema/login/login_esqueceu_senha_novasenha', $viewData);
    }

    public function newPassword($token)
    {
        $viewData = array(
            'token'   => $token
        );

        try {
            $usuario = RepositoryFactory::make('usuarios')->findOneBy(
                array(
                    new QueryCriteria('tokenacesso', $token),
                    new QueryCriteria('ativo', true),
                )
            );

            $viewData['name']   = $usuario->nome;
            $viewData['email']  = $usuario->email;

            if ($this->input->post()) {
                $data = $this->input->post();
                self::validateRequestDataPasswordsMatches();

                $usuario->alterarSenha = '1';
                $usuario->senha = $data['senha'];
                $usuario->confirmasenha = $data['confirmasenha'];

                $this->usuarioRepository->update(get_object_vars($usuario));

                redirect('sistema/dashboard');
                return true;
            }

        } catch (\Exception $error) {
            $viewData['errorMessage'] = $error->getMessage();

            $this->twigError('sistema/login/login_nova_senha', $viewData);
            return false;
        }

        $this->twigDisplay('sistema/login/login_nova_senha', $viewData);
    }

    private function validateRequestDataForgotPassword()
    {
        $rules = CodeIgniterValidationRulesBuilder::getInstance()
            ->clearRules()
            ->setRules('email', 'E-mail', array('trim', 'required', 'valid_email'))
            ->getRules();

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === false) {
            throw new RuntimeException(validation_errors());
        }
    }

    private function validateRequestDataPasswordsMatches()
    {
        $rules = CodeIgniterValidationRulesBuilder::getInstance()
            ->clearRules()
            ->setRules('senha', 'Senha', array('required'))
            ->setRules('confirmasenha', 'Confirmar Senha', array('required', 'matches[senha]'))
            ->getRules();

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === false) {
            throw new RuntimeException(validation_errors());
        }
    }
}