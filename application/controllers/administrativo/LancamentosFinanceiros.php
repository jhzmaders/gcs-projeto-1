<?php

use toshyro\gcs\mvc\BaseController;
use toshyro\gcs\repository\IRepository;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;
use toshyro\gcs\service\MailgunService;

class LancamentosFinanceiros extends BaseController
{

    public function index()
    {
        $viewData['lancamentosFinanceiros'] = $this->getRepository()->findAll(array());

        $this->twigDisplay('administrativo/lancamentosFinanceiros/lancamentos_financeiros_list', $viewData);
    }

    public function add()
    {
        $viewData[] = array();

        if ($this->input->post()) {
            try {

                $data = $this->input->post();

                $data['datavencimento'] = formatDateForDB($data['datavencimento']);
                $data['dataregistro']   = formatDateForDB(date('d/m/Y'));
                $data['valor']          = formatDecimalForDB($data['valor']);

                $id = $this->getRepository()->insert($data);

                redirect('administrativo/lancamentosFinanceiros/edit/' . $id);
            } catch (Exception $e) {
                $viewData['errorMessage'] = $e->getMessage();
                $this->twigError('administrativo/lancamentosFinanceiros/lancamentos_financeiros_add', $viewData);

                return;
            }
        }

        $this->twigDisplay('administrativo/lancamentosFinanceiros/lancamentos_financeiros_add', $viewData);
    }

    public function edit($id)
    {
        if ($this->input->post()) {

            $data = $this->input->post();

            $data['datavencimento'] = formatDateForDB($data['datavencimento']);
            $data['dataregistro']   = formatDateForDB(date('d/m/Y'));
            $data['valor']          = formatDecimalForDB($data['valor']);

            $this->getRepository()->update($data);

            $viewData['serverSuccess'] = 'Dados atualizados com sucesso.';
        }

        $viewData['lancamentoFinanceiro'] = $this->getRepository()->findByID($id);

        $this->twigDisplay('administrativo/lancamentosFinanceiros/lancamentos_financeiros_edit', $viewData);
    }

    /** @return IRepository */
    private function getRepository()
    {
        return RepositoryFactory::make('lancamentosFinanceiros');
    }
}