var gulp = require('gulp'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    uglify = require('gulp-uglify');

var basePath = './bower_components/';
var destBaseDir = './public/';

var paths = {
    'jquery': basePath + 'jquery/dist/',
    'loading': basePath + 'jquery-loading/dist/',
    'materialize': basePath + 'materialize/dist/',
    'alertify': basePath + 'alertify-js/build/',
    'jquerymaskinput': basePath + 'jQuery-Mask-Plugin/dist/',
    'jquerymaskmoney': basePath + 'jquery.maskMoney/dist/',
    'fontawesome': basePath + 'font-awesome/',
    'datatables': basePath + 'datatables/media/',
    'selectize': basePath + 'selectize/dist/',
    'wysiwyg': basePath + 'wysiwyg/',
    'datepicker': basePath + 'datepicker/dist/',
    'sweetalert': basePath + 'bootstrap-sweetalert/dist/',
    'moment': basePath + 'moment/min/',
    'jquery_ui': basePath + 'jquery-ui/',
};

// Gulp plumber error handler
var onError = function (err) {
    console.log(err);
};

// Lets us type "gulp" on the command line and run all of our tasks
gulp.task('default', ['copyfiles', 'scripts', 'styles']);

// Copy fonts from a module outside of our project (like Bower)
gulp.task('copyfiles', function () {
    gulp.src([
        paths.fontawesome + "fonts/**/*.{ttf,woff,eof,svg,woff2,otf}",
    ])
        .pipe(gulp.dest(destBaseDir + 'fonts'));

    gulp.src([
        paths.datatables + "images/**/*.{png,psd}"
    ])
        .pipe(gulp.dest(destBaseDir + 'images'));

    //WYSIWYG
    gulp.src([
        paths.wysiwyg + '**/*.*'
    ])
        .pipe(gulp.dest(destBaseDir + 'js/plugins/wysiwyg'));
});

// Compress and minify images to reduce their file size
gulp.task('images', function () {
    var imgSrc = './src/images/**/*',
        imgDst = './images';

    return gulp.src(imgSrc)
               .pipe(plumber({
                   errorHandler: onError
               }))
               .pipe(changed(imgDst))
               .pipe(imagemin())
               .pipe(gulp.dest(imgDst))
               .pipe(notify({message: 'Images task complete'}));
});

// Hint all of our custom developed Javascript to make sure things are clean
gulp.task('jshint', function () {
    return gulp.src('./public/js/gcs-*.js')
               .pipe(plumber({
                   errorHandler: onError
               }))
               .pipe(jshint())
               .pipe(jshint.reporter('default'))
               .pipe(notify({message: 'JS Hinting task complete'}));
});

// Combine/Minify/Clean Javascript files
gulp.task('scripts', function () {
    return gulp.src([
        //JQUERY
        paths.jquery + 'jquery.min.js',
        //MATERIALIZECSS
        paths.materialize + 'js/materialize.min.js',
        //JQUERY LOADING
        paths.loading + 'jquery.loading.min.js',
        //ALERTIFY
        paths.alertify + 'alertify.min.js',
        //JQUERY MASK INPUT
        paths.jquerymaskinput + 'jquery.mask.min.js',
        //JQUERY MASK MONEY
        paths.jquerymaskmoney + 'jquery.maskMoney.js',
        //DATATABLES
        paths.datatables + 'js/jquery.dataTables.js',
        //SELECTIZE
        paths.selectize + 'js/standalone/selectize.min.js',
        //DATEPICKER
        paths.datepicker + 'datepicker.min.js',
        //MOMENT
        paths.moment + 'moment.min.js',
        //SWEETALERT
        paths.sweetalert + 'sweetalert.js',
        //jquery ui
        paths.jquery_ui + 'ui/minified/widget.js',
        paths.jquery_ui + 'ui/minified/data.js',
        paths.jquery_ui + 'ui/minified/scroll-parent.js',
        paths.jquery_ui + 'ui/widgets/mouse.js',
        paths.jquery_ui + 'ui/widgets/sortable.js',
    ]).pipe(plumber({
        errorHandler: onError
    })).pipe(
        concat('plugins.min.js')
        //).pipe(
        //  uglify()
    ).pipe(gulp.dest(destBaseDir + 'js'))
});

gulp.task('styles', function () {
    return gulp.src([
        //MATERIALIZECSS
        paths.materialize + 'css/materialize.min.css',
        //ALERTIFY
        paths.alertify + 'css/alertify.min.css',
        paths.alertify + 'css/themes/bootstrap.min.css',
        //FONT AWESOME
        paths.fontawesome + 'css/font-awesome.min.css',
        //DATATABLES
        paths.datatables + 'css/jquery.dataTables.min.css',
        //SELECTIZE
        paths.selectize + 'css/selectize.default.css',
        //DATEPICKER
        paths.datepicker + 'datepicker.min.css',
        //jquery ui
        paths.jquery_ui + 'themes/base/base.css',
        paths.jquery_ui + 'themes/base/theme.css',
        paths.jquery_ui + 'themes/base/sortable.css',
        //SWEETALERT
        paths.sweetalert + 'sweetalert.css',
    ])
               .pipe(plumber({
                   errorHandler: onError
               }))
               .pipe(concat('plugins.min.css'))
               .pipe(gulp.dest(destBaseDir + 'css'))
});


// This handles watching and running tasks as well as telling our LiveReload server to refresh things
gulp.task('watch', function () {
// Check for modifications in particular directories

// Whenever a stylesheet is changed, recompile
    gulp.watch('./src/sass/**/*.scss', ['styles']);

// If user-developed Javascript is modified, re-run our hinter and scripts tasks
    gulp.watch('./src/scripts/**/*.js', ['jshint', 'scripts']);

// If an image is modified, run our images task to compress images
    gulp.watch('./src/images/**/*', ['images']);

// Create a LiveReload server
    var server = liveReload();

// Watch any file for a change in the 'src' folder and reload as required
    gulp.watch(['./src/**']).on('change', function (file) {
        server.changed(file.path);
    })
});