<?php


use Phinx\Migration\AbstractMigration;

class CriaEstruturaInicial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $usuarios = array(
            array(
                'id'          => 1,
                'nome'        => 'Jonas',
                'email'       => 'jonas@hotmail.com',
                'senha'       => '$2y$10$Db49RatRNiB7bJdVT3hJFu5ACaQ3iDxomDuHlH9s.evUJxbR4Y5km',
                'tokenacesso' => null,
                'ativo'       => true,
            ),
        );

        $this->table('usuarios')
             ->addColumn('nome', 'string', array('limit' => 255, 'null' => false))
             ->addColumn('email', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('senha', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('tokenacesso', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('ativo', 'boolean', array('null' => false))
             ->insert($usuarios)
             ->save();

        $modulos = array(
            array(
                'id'         => 1,
                'descricao'  => 'Administrativo',
                'urisegment' => 'administrativo',
            ),
        );

        $this->table('modulos')
             ->addColumn('descricao', 'string', array('limit' => 255, 'null' => false))
             ->addColumn('urisegment', 'string', array('limit' => 255, 'null' => false))
             ->insert($modulos)
             ->save();

        $permissoes = array(
            array(
                'id'        => 1,
                'descricao' => 'Administrador',
                'valor'     => '1',
                'idmodulo'  => 1,
            ),
        );

        $this->table('permissoes')
             ->addColumn('descricao', 'string', array('limit' => 255, 'null' => false))
             ->addColumn('valor', 'string', array('limit' => 10, 'null' => false))
             ->addColumn('idmodulo', 'integer', array('null' => false))
             ->addForeignKey('idmodulo', 'modulos', 'id')
             ->insert($permissoes)
             ->save();

        $usuariospermissoes = array(
            array(
                'idusuario'   => 1,
                'idpermissao' => 1,
            ),
        );

        $this->table('usuariospermissoes')
             ->addColumn('idusuario', 'integer', array('null' => false))
             ->addColumn('idpermissao', 'integer', array('null' => false))
             ->addForeignKey('idusuario', 'usuarios', 'id')
             ->addForeignKey('idpermissao', 'permissoes', 'id')
             ->insert($usuariospermissoes)
             ->save();
    }

    public function down()
    {
        $this->dropTable('usuariospermissoes');
        $this->dropTable('usuarios');
        $this->dropTable('permissoes');
        $this->dropTable('modulos');
    }
}
