<?php

use toshyro\gcs\repository\IRepository;
use toshyro\gcs\repository\QueryCriteria;
use toshyro\gcs\repository\RepositoryFactory;

class UsuarioTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $idUsuarioCreated;

    /** @var IRepository $usuarioRepository */
    protected $usuarioRepository;

    /** @var IRepository $usuarioPermissaoRepository */
    protected $usuarioPermissaoRepository;

    protected function _before()
    {
        $this->usuarioRepository = RepositoryFactory::make('usuarios');
        $this->usuarioPermissaoRepository = RepositoryFactory::make('usuariospermissoes');

        $this->idUsuarioCreated = $this->usuarioRepository->findBy(null, array('id' => 'desc'))[0]->id;
    }

    protected function _after()
    {
    }

    // tests
    public function testUsuarioAdd()
    {
        try {
            $id = $this->usuarioRepository->insert($this->getModelUsuarioAdd());

            $this->assertEquals(true, $id > 0);
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testeUsuarioVerifyAdd()
    {
        try {
            $usuario = get_object_vars($this->usuarioRepository->findByID($this->idUsuarioCreated));

            $this->validateUsuarioToEquals($usuario, $this->getModelForVerifyUsuarioAdd());

        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testUsuarioEdit()
    {
        try {
            $this->usuarioRepository->insert($this->getModelUsuarioEdit());
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testUsuarioVerifyEdit()
    {
        try {
            $usuario = get_object_vars($this->usuarioRepository->findByID($this->idUsuarioCreated));

            $this->validateUsuarioToEquals($usuario, $this->getModelForVerifyUsuarioEdit());
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    public function testUsuarioDelete()
    {
        try{
            $this->usuarioPermissaoRepository->deleteBy(array(new QueryCriteria('idUsuario', $this->idUsuarioCreated)));
            $this->usuarioRepository->deleteByID($this->idUsuarioCreated);

            $lancamentoFinanciero = $this->usuarioRepository->deleteByID($this->idUsuarioCreated);

            $this->assertEquals(null, $lancamentoFinanciero);
        } catch (Exception $e) {
            $this->fail('Exception error: ' . $e->getMessage());
        }
    }

    private function getModelUsuarioAdd()
    {
        $usuario = array();

        $usuario['nome']          = "testeAutomatizado";
        $usuario['email']         = "teste@automatizado.com";
        $usuario['modulo']        = 1;
        $usuario['permissao']     = 1;
        $usuario['permissoes']    = array(1);
        $usuario['gerarToken']    = 2;
        $usuario['senha']         = 123;
        $usuario['confirmasenha'] = 123;

        return $usuario;
    }

    private function getModelUsuarioEdit()
    {
        $usuario = array();

        $usuario['id']         = $this->idUsuarioCreated;
        $usuario['nome']       = "testeAutomatizado EDITED";
        $usuario['email']      = "teste@automatizado.com";
        $usuario['modulo']     = 1;
        $usuario['permissao']  = 1;
        $usuario['permissoes'] = array(1);

        return $usuario;
    }

    public function getModelForVerifyUsuarioAdd()
    {
        $usuario = array();

        $usuario['id']          = $this->idUsuarioCreated;
        $usuario['nome']        = "testeAutomatizado";
        $usuario['email']       = "teste@automatizado.com";
        $usuario['senha']       = password_hash('123', PASSWORD_DEFAULT);
        $usuario['tokenacesso'] = null;
        $usuario['ativo']       = true;

        return $usuario;

    }

    public function getModelForVerifyUsuarioEdit()
    {
        $usuario = array();

        $usuario['id']          = $this->idUsuarioCreated;
        $usuario['nome']        = "testeAutomatizado EDITED";
        $usuario['email']       = "teste@automatizado.com";
        $usuario['senha']       = password_hash('123', PASSWORD_DEFAULT);
        $usuario['tokenacesso'] = null;
        $usuario['ativo']       = true;

        return $usuario;

    }

    private function validateUsuarioToEquals($usuario1, $usuario2)
    {
        foreach ($usuario1 as $key => $field) {
            if ($key === "senha") {
                continue;
            }

            if ($field != $usuario2[$key]) {
                $this->fail(sprintf('Exception error: field %s = %s is different to field %s = %s ', $key, $field, $key,
                    $usuario2[$key]));
            }
        }
    }
}