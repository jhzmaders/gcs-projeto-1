# Branches
O branch principal de desenvolvimento é o `master`. Ele está protegido para commits, mas todos os novos branches devem partir dele ou de outros branches criados a partir dele quando necessário.

# Configuração
Após baixar os fontes, é preciso realizar as seguintes configurações (no diretório da raiz da aplicação - {APPDIR}):
- Executar o comando `composer install`
- Executar o comando `npm install`
- Executar o comando `bower install`
- Executar o comando `gulp`

## Permissão de escrita
É preciso que o apache tenha permissão de escrita nas pastas: (em ambientes Windows isso normalmente está OK)
- {APPDIR}/application/cache
- {APPDIR}/application/logs
- {APPDIR}/application/sessions

## Arquivo .env
Na pasta {APPDIR} existe um arquivo chamado `.env.example`, que é um modelo para o arquivo a ser criado na mesma pasta, mas como o nome `.env`
Nesse arquivo vão as seguintes configurações:
- dbname = nome do banco de dados
- dbuser = nome do usuário de banco de dados
- dbpass = senha do usuário do banco de dados
- dbhost = IP ou nome do servidor de banco de dados
- email = e-mail para receber as mensagens quando em desenvolvimento/teste
- mailgun_apikey = key-6b5721062691f83debdfd6c439b1d8cb
- mailgun_domain = key-6b5721062691f83debdfd6c439b1d8cb
- tagfile = arquivo contendo versão do sistema

A apikey e domain do mailgun podem ser copiados. Eles não mudam.

## Configuração do apache
Essa configuração varia em cada ambiente, mas as orientações gerais são:
- Criar um virtualhost cujo DocumentRoot aponte para a pasta {APPDIR}/public e com ServerName customizado (exemplo: `ServerName gcs.local`)
- Criar a entrada definida em ServerName no arquivo HOSTS