(function ($, gcs) {

    function forgotPassword()
    {
        var email = $("[name=login]").val();

        if(!email){
            alertify.error("Preencha o email do usuario que quer recurapar no campo e-mail");
            return;
        }

        swal({
            title: 'Esqueceu a senha?',
            text: '<p>Enviaremos uma senha para <span style="color: black; font-weight: bold">' + email + '</span>.</p><p>Deseja continuar?</p>',
            html: true,
            confirmButtonText: "Sim, enviar!",
            cancelButtonText: "Cancelar",
            showCancelButton: true,
        }, function () {
            var postData = {
                email: email
            };

            gcs.post('sistema/login/forgotPassword', postData, function (response) {
                if (response.errors) {
                    alertify.error(response.errors);
                    return;
                }

                if (response.data.serverSuccess) {
                    alertify.success(response.data.serverSuccess);
                    return;
                }

                return;
            });
        });
    }

    $(document).on("click", "#passwordRecovery", function (e) {
        e.preventDefault();
        forgotPassword();
    });

}(jQuery, gcs));
